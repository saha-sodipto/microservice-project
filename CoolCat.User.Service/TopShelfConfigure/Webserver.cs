﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolCat.User.Service.TopShelfConfigure
{
    public class Webserver
    {
        private IDisposable _webapp;

        public void Start()
        {
            string BaseAddress = "http://localhost:8081";
            _webapp=WebApp.Start<Startup>(BaseAddress);
        }

        public void Stop()
        {
            _webapp?.Dispose();
        }
    }
}
