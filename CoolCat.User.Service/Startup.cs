﻿using CoolCat.User.Service.App_Start;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CoolCat.User.Service
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();

            //Route Setting
            WebApiConfig.Register(config);

            appBuilder.UseWebApi(config);
        }
    }
}
