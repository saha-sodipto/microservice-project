﻿using CoolCat.User.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace CoolCat.User.Service.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        [Route("")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            List<user> users = new List<user>()
            {
                new user() {id=1,name="Sodipto Saha" },
                new user() {id=2,name="Badhan Saha" }

            };
            return Ok(users);
        }

        [Route("{id:int}/info")]
        [HttpGet]
        public IHttpActionResult GetUserInfo(int id)
        {
            List<user> users = new List<user>()
            {
                new user() {id=1,name="Sodipto Saha" },
                new user() {id=2,name="Badhan Saha" }

            };
            var userInfo = users.Where(x => x.id == id).FirstOrDefault();
            if (userInfo != null)
            {
                return Ok(userInfo);
            }
            else
            {
                return NotFound();
            }

        }
    }
}
