﻿using CoolCat.User.Service.TopShelfConfigure;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace CoolCat.User.Service
{
    class Program
    {
        static void Main(string[] args)
        {

            //Start Owin service

            //string BaseAddress = "http://localhost:8081";  //User any port ex: 8080
            //WebApp.Start<Startup>(BaseAddress);

            //Console.WriteLine("User Service Started");
            //Console.WriteLine("BaseAddress:" +BaseAddress);

            //Using TopShelf to help run this service inside windows service. ()
            TopshelfStart();

            Console.ReadLine();

        }

        static void TopshelfStart()
        {
            HostFactory.Run(x =>
            {
                x.Service<Webserver>(s =>
                {
                    s.ConstructUsing(name=>new Webserver());
                    s.WhenStarted(tp => tp.Start());
                    s.WhenStopped(tp => tp.Stop());

                });
                x.RunAsLocalSystem();
                x.SetDescription("MicroService web api Project with self hosted by owin ");
                x.SetDisplayName("Coolcat UserService");
                x.SetServiceName("CoolcatUserService");

            });
        }
    }
}
